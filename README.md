# app and api multi file upload
   This codebase is a simple page which allows to upload multiple files to a postgres database via REST API.
   
   Each file is stored using Postgres *bytea* storage and has an associated "identifier" form field that is saved in the database with the file data and atributes.
   API are designed a modular architecture.
   
   
##Interface

The simple page used to send the files is developed using Boostrap 3, jQuery and Handlebars.
The user can decide how many files to upload by select menù.

The max number of file could be set in page configuration. 




### Technologies and Dependencies

* Node v6.5 (ES6)
* Express 4
* Postgres
* Sequelize
* Multiparty
* body-parser
* NPM
* HTML5
* jQuery
* Ajax
* Handlebars


### Quick start (Only some steps)

Clone the repository

```
$ git clone https://gitlab.com/AddyArdau/app-multi-file-upload
```

Before installing this app you need to configure your database filling **database** attributes of the file

**api/config/config.json**
```
"database": {
  "username": "postgres",
  "password": "",
  "database": "",
  "host": "127.0.0.1",
  "dialect": "postgres",
  "port": 5432
}
```

You can set also other configuration properties of the app inside this file. 


**Install the software with npm**
```
$ npm install
```


**Start with node,js**
```
node server.js
```


