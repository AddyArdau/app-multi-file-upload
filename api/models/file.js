'use strict';
module.exports = (sequelize, DataTypes) => {
  const File = sequelize.define('File', {
    filename: DataTypes.STRING,
    identifier: DataTypes.STRING,
    type: DataTypes.STRING,
    size: DataTypes.INTEGER,
    data: DataTypes.BLOB
  }, {});
  File.associate = function(models) {
  };
  return File;
};