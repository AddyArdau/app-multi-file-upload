const _ = require('lodash');
const fs = require('fs');
const File = require('../models').File;
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];

let apiConfig = {
  port: 3000,
  host: "localohst",
  apiUrl: "/api/v1"
};

if (config.hasOwnProperty('app')) {
  apiConfig = Object.assign(apiConfig, config.app);
}

exports.file_get_all = (req, res, next) => {

  File
    .findAll()
    .then(results => {

      if (!results) {
        console.log("No file saved");
        return res.status(400).json({
          error: "No file saved"
        });
      }

      const response = {
        count: results.length,
        data: results.map(result => {
          return {
            filename: result.filename,
            identifier: result.identifier,
            type: result.type,
            size: result.size,
            id: result.id,
            urlData: "http://" + apiConfig.host + ":" + apiConfig.port + apiConfig.apiUrl + "/file/download/" + result.id,
            request: {
              type: "GET",
              url: "http://" + apiConfig.host + ":" + apiConfig.port + apiConfig.apiUrl + "/file/" + result.id
            }
          };
        })
      };

      res.status(200).json(response);
    })
    .catch(err => {
      console.log(err);
      res.status(400).json({
        error: err
      });
    });
};

exports.file_create_file = (req, res, next) => {

  if (!('files' in req)) {
    console.log("No file uploaded error");
    res.sendStatus(400);
  }

  let filesList = Object.keys(req.files).map(function(key) {
    if (req.files[key] && req.files[key].name) {
      return req.files[key];
    }
  }).filter(function(file) {
    return (file && file.name);
  });

  const readAndUpload = (file) => { // sample async action
    return new Promise(function(resolve, reject) {

      return fs.readFile(file.path, function(err, data) {
        return File.create({
          filename: file.name,
          identifier: file.fieldName,
          data: data,
          type: file.type,
          size: file.size
        }).then(fileData => {
          return fs.unlink(file.path, err => {
            if (err) {
              console.log("File can not be deleted");
              return reject(err);
            }

            return resolve(fileData);
          });
        }).catch(err => {
          console.log(err);

          return reject(err);
        });
      });
    });
  };

  return Promise.all(filesList.map(readAndUpload)).then(results => {

    let dataResult = [];
    if (Array.isArray(results) && results.length > 0) {
      dataResult = results.map(function(result) {
        return {
          filename: result.filename,
          identifier: result.identifier,
          type: result.type,
          size: result.size,
          id: result.id,
          urlData: "http://" + apiConfig.host + ":" + apiConfig.port + apiConfig.apiUrl + "/file/download/" + result.id,
          request: {
            type: "GET",
            url: "http://" + apiConfig.host + ":" + apiConfig.port + apiConfig.apiUrl + "/file/" + result.id
          }
        }
      });
    }

    res.status(201).json({
      message: "Files uploaded successfully",
      data: dataResult
    })
  })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.file_get_file = (req, res, next) => {
  const id = req.params.fileId;
  File.findById(id)
    .then(result => {
      console.log("From database", result);
      if (result) {
        res.status(200).json({
          data: result,
          request: {
            type: "GET",
            url: "http://" + apiConfig.host + ":" + apiConfig.port + apiConfig.apiUrl + "/file"
          }
        });
      } else {
        res
          .status(404)
          .json({message: "No valid entry found for provided ID"});
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({error: err});
    });
};

exports.file_delete = (req, res, next) => {
  const id = req.params.fileId;
  File.remove({id: id})
    .then(result => {
      res.status(202).json({
        message: "File deleted",
        request: {
          type: "DELETE",
          url: "http://" + apiConfig.host + ":" + apiConfig.port + apiConfig.apiUrl + "/file/" + id
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
