const express = require("express");
const router = express.Router();
const FileController = require('../controllers/file');
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();
const models  = require('../models');

router.get("/", FileController.file_get_all);

router.post("/", multipartMiddleware, FileController.file_create_file);

router.get("/:fileId", FileController.file_get_file);

router.delete("/:fileId", FileController.file_delete);

module.exports = router;
