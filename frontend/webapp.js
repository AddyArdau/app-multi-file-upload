var config = {
  maxFileFieldsNumber: 10,
  startFileFieldsNumber: 1,
  apiUrl: '/api/v1'
};

var state = {
  fileFieldsNumber: config.startFileFieldsNumber
};

$(document).ready(function() {
  hideMessages();
  initSelectNumber();
  initFileFiled();
  onUploadForm();
});


function hideMessages() {
  $('#alert-success').addClass('hide');
  $('#alert-warning').addClass('hide');
}

function onUploadForm() {
  $('#uploadForm').submit(function(e) {
    e.preventDefault();
    var form_data = new FormData(this);
    hideMessages();
    $.ajax({
      type: 'post',
      url:  config.apiUrl + '/file',
      data: form_data,
      cache: false,
      contentType: false,
      processData: false,
      success: function(responses) {
        $('#alert-success').removeClass('hide');

        if (!responses || !responses.hasOwnProperty('data') || !Array.isArray(responses.data) ||  responses.data.length == 0){
          var source = document.getElementById("alert-warning").innerHTML;
          var template = Handlebars.compile(source);
          var html = template({error: "No file uploaded"});
          $('#message-response').html(html);
        }

        var numberOfUploaded = responses.data.length;
        var source = document.getElementById("alert-success").innerHTML;
        var template = Handlebars.compile(source);
        var html = template({fileFields: responses.data, number: numberOfUploaded});
        $('#message-response').html(html);
        initFileFiled();
      },
      error: function(error) {
        var source = document.getElementById("alert-warning").innerHTML;
        var template = Handlebars.compile(source);
        var html = template({error: "Error uploading files"});
        $('#message-response').html(html);
      }
    })
  })
}

function initSelectNumber() {
  Handlebars.registerHelper('repeat', function(times, block) {
    var html = '';

    for (var index = 1; index <= times; ++index) {
      html += block.fn(index);
    }

    return html;
  });

  var source = document.getElementById("number-of-fields-template").innerHTML;
  var template = Handlebars.compile(source);
  var html = template({number: config.maxFileFieldsNumber});

  $("#number-of-fields").html(html);
  $("#number-of-fields").val(config.startFileFieldsNumber);
  $("#number-of-fields").change(function() {
    hideMessages();
    addFileFileds($(this).val());
  });
}

function initFileFiled() {
  var html = getFileFiledHtml(state.fileFieldsNumber);

  $("#files-container").html(html);
}

function createFileFiledsData(numberOfFields, startIndex) {

  var fileFields = [];

  if(typeof startIndex === 'undefined')  {
    startIndex = 1;
  }

  var lastIndex = numberOfFields + startIndex - 1;

  for (var index = startIndex; index <= lastIndex; ++index) {
    fileFields.push({index: index, title: "File " + index, uuid: getUuid()})
  }

  return fileFields;
}

function addFileFileds(fileFieldsNumber) {
  var html = '';
  var addFieldsNumber = 0;
  var startIndex = 1;
  fileFieldsNumber = parseInt(fileFieldsNumber);

  if (fileFieldsNumber == state.fileFieldsNumber) {
    return;
  }

  if (fileFieldsNumber > state.fileFieldsNumber) {
    addFieldsNumber = fileFieldsNumber - state.fileFieldsNumber;
    startIndex = state.fileFieldsNumber + 1;
    state.fileFieldsNumber = fileFieldsNumber;
    html = getFileFiledHtml(addFieldsNumber, startIndex);

    $(html).appendTo("#files-container");
  }

  if (fileFieldsNumber < state.fileFieldsNumber) {
    var removeNumber = state.fileFieldsNumber - fileFieldsNumber;
    var sliceIndex = removeNumber * (-1);
    $(".box").slice(sliceIndex).remove();
    state.fileFieldsNumber = fileFieldsNumber;
  }

}

function getFileFiledHtml(fileFieldsNumber, startIndex) {
  var fileFieldsData = {
    fileFields: createFileFiledsData(fileFieldsNumber, startIndex)
  };
  var source = document.getElementById("file-template").innerHTML;
  var template = Handlebars.compile(source);

  return template(fileFieldsData);
}

function getUuid() {
  var uuid = "", i, random;
  for (i = 0; i < 32; i++) {
    random = Math.random() * 16 | 0;

    if (i == 8 || i == 12 || i == 16 || i == 20) {
      uuid += "-"
    }
    uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
  }

  return uuid;
}
